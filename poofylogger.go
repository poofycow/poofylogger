package poofylogger

import (
	"fmt"
	"os"

	"time"

	"github.com/slf4go/logger"
)

const (
	colorPanic  = "\033[101m\033[1;30m"
	colorError  = "\033[0;91m"
	colorWarn   = "\033[0;93m"
	colorNotice = "\033[0;96m"
	colorInfo   = "\033[0;92m"
	colorDebug  = "\033[0;95m"
	colorTrace  = "\033[0;90m"
	colorText   = "\033[0;97m"
	colorReset  = "\033[0m" //Used for resetting colors
)

type PoofycowLogger struct {
	file *os.File
}

func (l *PoofycowLogger) SetFile(path string) error {
	file, err := os.Create(path)
	if err != nil {
		return err
	}

	l.file = file

	return nil
}

func (l *PoofycowLogger) Log(level logger.Level, msg string, stack []string) {
	var out *os.File
	if level <= logger.LogWarn {
		out = os.Stderr
	} else {
		out = os.Stdout
	}

	writeLog(out, level, msg, stack)

	if l.file != nil {
		writeLog(l.file, level, msg, stack)
	}
}

func writeLog(out *os.File, level logger.Level, msg string, stack []string) error {
	t := time.Now()
	tString := t.Format("2006-01-02 15:04:05")

	_, err := fmt.Fprintf(out, "%s[%s]%s %s %s%s\n", getColor(level), logger.LevelName(level), colorText, tString, msg, colorReset)
	if err != nil {
		return err
	}

	if stack != nil {
		for i, line := range stack {
			_, err := fmt.Fprintf(out, "%s[%s]%s %s %d: %s%s\n", getColor(level), logger.LevelName(level), colorText, tString, len(stack)-i-1, line, colorReset)

			if err != nil {
				return err
			}
		}
	}

	return nil
}

func getColor(level logger.Level) string {
	switch level {
	case logger.LogPanic:
		return colorPanic
	case logger.LogError:
		return colorError
	case logger.LogWarn:
		return colorWarn
	case logger.LogNotice:
		return colorNotice
	case logger.LogDebug:
		return colorDebug
	case logger.LogTrace:
		return colorTrace
	default:
	case logger.LogInfo:
		return colorInfo
	}

	return colorInfo
}

func (PoofycowLogger) SetLevel(level logger.Level) {}
